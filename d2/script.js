// additional discussion to JSON

// create a function called messgeToSelf() - display a message to your past self in our console. Invoke the function 10 times

function msgToSelf() {
	console.log("Don't text her back.");
}

// While Loop
	let count = 10;

	while (count !== 0) {
		msgToSelf();
		count--;
	}

// display numbers from 1-5 (in order)
console.log(`display numbers from 1-5 (in order)`);
let x=0;
	while (x <= 5) {
		console.log(x);
		x++;
	}

// Do-while Loop
	// is similar to the while loop. However, do while will allow us to run our loop at least once.


	// while -> we check the condition first before running our codeblock
	// Do-While -> it will do an instruction first before it will check the condition to run again



// Creat a do-while loop which will be able to show the numbers in the console from 1-20 order

let counter = 1;

do {
	console.log(counter);
	counter++;
} while (counter <= 20);



// For Loop
	// initialization; condition; final expression
let fruits = ["Apple", "Mango", "Pineapple", "Guyabano"]

for (let i=0; i <= fruits.length; i++) {
	console.log(fruits[i]);
}




//Global/Local Scope

	// Global Scope
			//are variables can be accessed inside a functin or anywhere else in our script

		let userName = 'super_knight_1000';
		// an example of global loop

		function sample() {
			//local scoped or function scoped variable
			let heroName = "One Punch Man"
			//this local var cannot be accessed outside the function it was created from
			console.log(heroName);
			console.log(userName);
		}

		sample();
		console.log(userName);

// Strings are similar to Arrays

let powerpuffGirls = ['Blossom', 'Bubbles', 'Buttercup'];

console.log(powerpuffGirls[0]);
	// output: Blossom

let name = "Alexandro";

console.log(name[0]);
	// output: A

console.log(name[name.length-1]);
	// output: o -> to get the last index




// Mini-Activity
	// display a stings indeces
	function stringBroker (string) {
		for (let i=0; i <= string.length; i++) {
			console.log(string[i]);
		}
	}

	stringBroker('Zuitt');

	// display as array
	function stringBrokerArr (string) {
		let stringArr = new Array;
		for (let i=0; i <= string.length; i++) {
			stringArr.push(string[i]);
		}
		console.log(stringArr);
	}

	stringBrokerArr('Zuitt');
