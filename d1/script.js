// JSON OBJECTS
// JSON stands for Javascript Object Notation

// JSON is used for serializing different data types into bytes

// Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information

// bytes =? binary digits (1 and 0) that is used to represent a character

/*
Jason Data format
	Syntax of a JSON Object:

		{
			"propertyA": "valueA",
			"propertyB": "valueB",
			"propertyN": "valueN",
		}

	differnce to JS Object:
		// no qoutation marks
	JS Object
		{
			//var : //strings
			city: "QC",
			province: "Metro Manila",
			country: 'Philippines'
		}

	JSON Object (must be a string)
		{
			//string : //string
			"city": "QC",
			"province": "Metro Manila",
			"country": 'Philippines'
		}
	NOTE: JSON reads all of data as string, and converts it to bytes.


// JSON Array
	"cities": [
		{
			"city": "QC",
				"province": "Metro Manila",
				"country": 'Philippines'
		}
		{
			"city": "QC",
				"province": "Metro Manila",
				"country": "Philippines"
		}
		{
			"city": "QC",
				"province": "Metro Manila",
				"country": "Philippines"
		}
	]

*/


// JSON  Methods
	// JSON contains methods for parsing and converting data into stringified JSON

	/*
		Stringified JSON is a javascript object converted into a string to be used in other fuction of a Javascript application
	*/

// an array of objects
let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	}
]


// Stringify Method is used to convert JS Objects into JSON(String)
console.log('Result form sringify method')
console.log(JSON.stringify(batchesArr))

	// Output: [{"batchName":"Batch X"},{"batchName":"Batch Y"}]

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data)

	/* Output
		{"name":"John","age":31,"address":{"city":"Manila","country":"Philippines"}}
	*/


// Mini-Activity
	// use the prompt method in order to gather user data to be supplied to the user details, then once the data gathered, convert it into stringify and print the details
		// user details
		/* 
			firstName:
			lastName:
			age:
			address: city, country, zip code

		*/



let firstName = prompt("First Name: ")
let lastName = prompt("Last Name: ")
let age = prompt("Age: ")
let address = [
		prompt("City: "), 
		prompt("Country: "), 
		prompt("Zip Code: ")
	]


let userDetails = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(userDetails)

//Converting Stringified JSON into JS Objects

// Information is commonly sent to applictions in stringified JSON  and then converted back into objects. 

// This happens both for sending information to a backend app and sending information back to frontend app

// Upon receiving data, JSON text can be converted to a JS object with parse

let batchesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch X"
	}
]`

console.log('Results from parse method: ')
console.log(JSON.parse(batchesJSON))


let stringifiedObject = `[
	{
		"name": "John",
		"age": "31",
		"address": {
			"city": "Manila",
			"country": "Philippines"
		}
	}
]`

console.log(JSON.parse(stringifiedObject))


//////////  SUMMARY  //////////

// JSON.STRINGFY()
/* 
	Send data from CLIENT to SERVER or change object to JSON

	strings are being sent
*/

// JSON.PARSE()
/*
	recieving data from SERVER to CLIENT
	
	data is converted back to Object
*/


